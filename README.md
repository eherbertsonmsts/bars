# bars

    ┌──────────────────────────────────────────────────────┐
    │  65789.80 ┐    ▄                         ┌  65789.80 │
    │           │  █ █                         │           │
    │  57566.08 ┼  █ █    █▄█                  ┼  57566.08 │
    │           │  ███    ███▄▄                │           │
    │  49342.35 ┼  ███    ██████  █            ┼  49342.35 │
    │           │  ███    ██████  █            │           │
    │  41118.62 ┼  ███    ██████  █            ┼  41118.62 │
    │           │  ███▄   ██████  █            │           │
    │  32894.90 ┼  ████▄█▄██████  █            ┼  32894.90 │
    │           │ ▄██████████████▄█▄ ▄       ▄ │           │
    │  24671.18 ┼ ██████████████████ █▄█▄▄ ▄██ ┼  24671.18 │
    │           │ ████████████████████████████ │           │
    │  16447.45 ┼ ████████████████████████████ ┼  16447.45 │
    │           │ ████████████████████████████ │           │
    │   8223.72 ┼ ████████████████████████████ ┼   8223.72 │
    │           │ ████████████████████████████ │           │
    │      0.00 ┘ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ └      0.00 │
    └──────────────────────────────────────────────────────┘

Perl script to produce a bar chart on stdout, given whitespace-separated numeric input on stdin

## Usage

    echo '3 4 10 2 -1 -5 -18' | bars
    cat numbers.txt | SCALE=10 BAR_WIDTH=2 ASCII=1 bars 

## Why are there horizontal blank lines all over my chart?!?

Because your text editor, terminal emulator, or other text renderer is adding
extra vertical space between lines of text. Frequently, there's not going to be
anything you can do about it, although sometimes you can adjust something
called "line spacing" to reduce the severity. Sometimes switching fonts can
help.

## Environment Variables

If `LATIN1` is set, `bars` will use ISO-8859-1 encoding when producing output,
in case you're in encoding hell. It won't be as pretty as using Unicode
box-drawing characters (which is the default), but it should be usable.

If `ASCII` is set, `bars` will use only single-byte characters, in case you're
*really* in encoding hell. (This setting trumps the `LATIN1` setting, if both
are set.)

If `SCALE` is set, `bars` will adjust the height of the charts it creates
accordingly, with the number given being the target number of scaling intervals
(i.e. one less than the number of "ticks" on the y-axis). By default `bars`
will use a scale of 15 (= 16 "ticks" on the y-axis).

If `BAR_WIDTH` is set, `bars` will adjust the width of each bar in its charts
accordingly. By default each bar is one character wide.

## Compatibility

This script understand numbers in several formats, including decimals and
exponential notation:

    1031.058
    -0.251
    135.6e4

Your input can be either space-, tab-, or newline-separated, or any combination
of them.

## Precision

The precision of the charts is generally okay for many purposes, but ultimately
is limited to `2^((SCALE * 2)+1)` bits. This is because `bars` only uses
half-height and full-height box shading characters. Therefore, values that are
sufficiently close to one another result in bars that are *exactly* the same
height.

## Limitations & Known Issues

There's a known issue where sometimes the "x-axis" (the region of the chart
near 0 on the y-axis) is not rendered properly, and is off-by-one. I've not
been able to bottom out exactly why or when it happens.
